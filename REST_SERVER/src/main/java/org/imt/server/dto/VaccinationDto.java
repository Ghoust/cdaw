package org.imt.server.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class VaccinationDto {

	private String country;
	private int vaccinated;
	
}
