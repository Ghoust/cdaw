package org.imt.server.controller;

import java.util.List;

import org.imt.server.dto.VaccinationDto;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/")
public class VaccinationController {

	private List<VaccinationDto> list = List.of(
			new VaccinationDto("FR", 450),
			new VaccinationDto("DE", 36000),
			new VaccinationDto("US", 180000)
	);
	
	@GetMapping("/")
	public List<VaccinationDto> getAll() {
		return list;
	}
	
	@GetMapping("/{country}")
	public VaccinationDto getByCountry(@PathVariable String country) {
		VaccinationDto dto = null;
		for (VaccinationDto vaccination : list) {
			if (vaccination.getCountry().contains(country)) {
				dto = vaccination;
			}
		}
		return dto;
	}
	
}
