<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>TP AJAX</title>
<script type="text/javascript" src="js/jquery-3.1.1.js"> </script>
</head>
<body>
	<select name='country' id='country'>
		<option value="Allemagne">Allemagne</option>
		<option value="France">France</option>
		<option value="USA">USA</option>
	</select>
	<input type="submit" id="search" value="GO"></input>
	
	<div id="covid">
		Taux de Covid
	</div>
	
	<script>
        $(document).ready(function() {
            $("#search").click(function() {
                var country = $("#country").val();
                console.log(country);
                $.getJSON("AjaxJson?country=" + country, function (data) {
                    console.log("data");
                    var countryData = data.content;
                    $("#covid").html("<p> Cas dans le pays " + country + " : <br>" + countryData + "</p>");
                });
            });
        });
    </script>
</body>
</html>