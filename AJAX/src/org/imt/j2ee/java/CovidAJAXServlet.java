package org.imt.j2ee.java;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.AbstractMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class AjaxJsonServlet
 */
@WebServlet(name = "AjaxJson", urlPatterns = { "/AjaxJson" })
public class CovidAJAXServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private Map<String, Long> mapCase = Map.ofEntries(
	  new AbstractMap.SimpleEntry<String, Long>("USA", (long) 15000000),
	  new AbstractMap.SimpleEntry<String, Long>("ALLEMAGNE", (long) 3000000),
	  new AbstractMap.SimpleEntry<String, Long>("FRANCE", (long) 2376852)
	);   
	
    /**
     * @see HttpServlet#HttpServlet()
     */
    public CovidAJAXServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("application/json");
		PrintWriter writer = response.getWriter();
		String parameter = request.getParameter("country");
		Long count = mapCase.get(parameter.toUpperCase());
		writer.println("{\"content\" : \"" + count.toString() + "\"");
		writer.println("}");
	}

}
