package com.imt.microservices.iban.dto;

import lombok.Data;

@Data
public class Form {

    private String iban;

}
