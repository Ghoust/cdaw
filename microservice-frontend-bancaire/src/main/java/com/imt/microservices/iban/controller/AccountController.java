package com.imt.microservices.iban.controller;

import java.io.IOException;
import java.util.List;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import com.imt.microservices.iban.dto.Account;
import com.imt.microservices.iban.dto.Transaction;
import com.imt.microservices.iban.service.AccountService;

@Slf4j
@Controller
@RequestMapping("/account")
public class AccountController {

	private final AccountService accountService;

	public AccountController(AccountService accountService) {
		this.accountService = accountService;
	}
	
	@GetMapping("/{account}")
	public String getTransactions(Model model, @PathVariable String account) {
		Account selectedAccount = null;
		List<Transaction> transactions = null;
		
		try {
			selectedAccount = accountService.getById(Long.parseLong(account));
			transactions = accountService.getTransactionsOfAccount(selectedAccount.getId());
		} catch (IOException | InterruptedException e) {
			log.error("Erreur lors de la récupération des transactions", e);
		}
		
		model.addAttribute("selectedAccount", selectedAccount.getIban());
		model.addAttribute("transactions", transactions);
		return "account";
	}
}
