package com.imt.microservices.iban.service;

import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpRequest.BodyPublishers;
import java.net.http.HttpResponse;
import java.util.List;

import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.imt.microservices.iban.dto.Account;
import com.imt.microservices.iban.dto.Transaction;

@Service
public class AccountService {

	private static final String API_URL = "http://localhost:8082/";

	public List<Account> getAll() throws IOException, InterruptedException {
		HttpClient client = HttpClient.newHttpClient();
		HttpRequest request = HttpRequest.newBuilder()
				.GET()
				.header("accept", "application/json")
				.uri(URI.create(API_URL + "/account/"))
				.build();
		HttpResponse<String> response = client.send(request, HttpResponse.BodyHandlers.ofString());

		ObjectMapper mapper = new ObjectMapper();
		return mapper.readValue(response.body(), new TypeReference<>() {});
	}
	
	public Account getById(Long id) throws IOException, InterruptedException {
		HttpClient client = HttpClient.newHttpClient();
		HttpRequest request = HttpRequest.newBuilder()
				.GET()
				.header("accept", "application/json")
				.uri(URI.create(API_URL + "/account/" + id))
				.build();
		HttpResponse<String> response = client.send(request, HttpResponse.BodyHandlers.ofString());

		ObjectMapper mapper = new ObjectMapper();
		return mapper.readValue(response.body(), new TypeReference<>() {});
	}
	
	public List<Transaction> getTransactionsOfAccount(Long id) throws IOException, InterruptedException {
		HttpClient client = HttpClient.newHttpClient();
		HttpRequest request = HttpRequest.newBuilder()
				.GET()
				.header("accept", "application/json")
				.uri(URI.create(API_URL + "/account/" + id + "/transactions"))
				.build();
		HttpResponse<String> response = client.send(request, HttpResponse.BodyHandlers.ofString());

		ObjectMapper mapper = new ObjectMapper();
		return mapper.readValue(response.body(), new TypeReference<>() {});
	}
	
	public void addAccount(String iban) throws IOException, InterruptedException {
		HttpClient client = HttpClient.newHttpClient();
		HttpRequest request = HttpRequest.newBuilder()
				.POST(BodyPublishers.ofString(""))
				.header("accept", "application/json")
				.uri(URI.create(API_URL + "/account/?iban=" + iban))
				.build();
		client.send(request, HttpResponse.BodyHandlers.ofString());
	}
	
	public void delete(Long id) throws IOException, InterruptedException {
		HttpClient client = HttpClient.newHttpClient();
		HttpRequest request = HttpRequest.newBuilder()
				.DELETE()
				.header("accept", "application/json")
				.uri(URI.create(API_URL + "/account/" + id))
				.build();
		client.send(request, HttpResponse.BodyHandlers.ofString());
	}
}
