package com.imt.microservices.iban.dto;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class Account {

	private Long id;
	private String iban;

	@JsonCreator
	public Account(
			@JsonProperty(value = "id", required = true) Long id,
			@JsonProperty(value = "iban", required = true) String iban) {
		this.id = id;
		this.iban = iban;
	}

	public Account() {
		// TODO Auto-generated constructor stub
	}
}
