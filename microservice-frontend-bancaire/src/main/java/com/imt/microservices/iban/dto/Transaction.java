package com.imt.microservices.iban.dto;

import java.math.BigDecimal;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class Transaction {

	private Long id;
	private BigDecimal amount;
	private Account debtor;
	private Account creditor;

	@JsonCreator
	public Transaction(
			@JsonProperty(value = "id", required = true) Long id,
			@JsonProperty(value = "amount", required = true) BigDecimal amount,
			@JsonProperty(value = "debtor", required = true) Account debtor,
			@JsonProperty(value = "creditor", required = true) Account creditor) {
		this.id = id;
		this.amount = amount;
		this.debtor = debtor;
		this.creditor = creditor;
	}

	public Transaction() {
		// TODO Auto-generated constructor stub
	}
}
