package com.imt.microservices.iban.controller;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.Collections;
import java.util.List;

import org.apache.commons.math3.random.RandomDataGenerator;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.imt.microservices.iban.MessagingConfig;
import com.imt.microservices.iban.dto.Account;
import com.imt.microservices.iban.dto.Form;
import com.imt.microservices.iban.dto.Transaction;
import com.imt.microservices.iban.service.AccountService;
import com.imt.microservices.iban.service.TransactionService;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Controller
@RequestMapping("/")
public class BancaireMVCController {

	private final AccountService accountService;
	private final TransactionService transactionService;
	
	@Autowired
	private RabbitTemplate rabbitTemplate;

	public BancaireMVCController(AccountService accountService, TransactionService transactionService) {
		this.accountService = accountService;
		this.transactionService = transactionService;
	}

	@GetMapping
	public String getAll(Model model) {
		List<Account> accounts = Collections.emptyList();
		try {
			accounts = accountService.getAll();
		} catch (IOException | InterruptedException e) {
			log.error("Erreur lors de la récupération des comptes enregistrés", e);
		}
		model.addAttribute("accounts", accounts);

		List<Transaction> transactions = Collections.emptyList();
		try {
			transactions = transactionService.getAll();
		} catch (IOException | InterruptedException e) {
			log.error("Erreur lors de la récupération des transactions", e);
		}
		model.addAttribute("transactions", transactions);
		
		return "index";
	}
	
	@PostMapping
	public String addAccount(@ModelAttribute Form form){
		try {
			accountService.addAccount(form.getIban());
		} catch (IOException | InterruptedException e) {
			log.error("Erreur lors de la création du compte {}", form.getIban(), e);
		}
		return "redirect:/";
	}
	
	@GetMapping("account/{account}/delete")
	public String delete(@PathVariable String account) {
		try {
			Account entity = accountService.getById(Long.parseLong(account));
			accountService.delete(entity.getId());
		} catch (IOException | InterruptedException e) {
			log.error("Erreur lors de la suppression du compte {}", account, e);
		}
		
		return "redirect:/";
	}
	
	@GetMapping("randomTransactions")
	public String addRandomTransactions(){
		Transaction transaction = new Transaction();
		List<Account> accounts = Collections.emptyList();
		Account account;
		try {
			account = accountService.getById(Long.parseLong("1"));
			accounts = accountService.getAll();
			for (int i = 0; i < 100; i++) {
				transaction.setCreditor(accounts.get(new RandomDataGenerator().nextInt(0,accounts.size()-1)));
				transaction.setDebtor(accounts.get(new RandomDataGenerator().nextInt(0,accounts.size()-1)));
				transaction.setAmount(BigDecimal.valueOf(new RandomDataGenerator().nextLong(0,50000)));
				rabbitTemplate.convertAndSend(MessagingConfig.EXCHANGE_NAME, MessagingConfig.ROUTING_KEY, transaction);
			}
		} catch (NumberFormatException | IOException | InterruptedException e) {
			e.printStackTrace();
		}
		
		return "redirect:/";
	}
}
