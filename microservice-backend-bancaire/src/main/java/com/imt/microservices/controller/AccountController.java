package com.imt.microservices.controller;

import com.imt.microservices.entity.Account;
import com.imt.microservices.service.AccountService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@Slf4j
@RestController
@RequestMapping("/account")
public class AccountController {

    private final AccountService service;

    public AccountController(AccountService service) {
        this.service = service;
    }

    @GetMapping("/")
    public List<Account> getAll() {
        return service.getAll();
    }
    
    @GetMapping("/{accountId}")
    public Optional<Account> getById(@PathVariable Long accountId) {
        return service.getById(accountId);
    }

    @PostMapping("/")
    public Account addAccount(@RequestParam String iban) {
        Account account = service.addAccount(iban);
        if(account == null) {
            log.warn("Le compte {} n'a pas été ajouté dans la BDD.", iban);
        }
        return account;
    }

    @DeleteMapping("/{accountId}")
    public void deleteAccount(@PathVariable Long accountId) {
        service.deleteAccount(accountId);
    }
}
