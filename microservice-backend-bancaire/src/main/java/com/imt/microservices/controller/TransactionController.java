package com.imt.microservices.controller;

import com.imt.microservices.entity.Transaction;
import com.imt.microservices.service.TransactionService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class TransactionController {

    private final TransactionService service;

    public TransactionController(TransactionService service) {
        this.service = service;
    }

    @GetMapping("/transactions")
    public List<Transaction> getAll() {
        return service.getAll();
    }

    @GetMapping("/account/{accountId}/transactions")
    public List<Transaction> getTransactionsByAccount(@PathVariable Long accountId) {
        return service.getTransactionsByAccount(accountId);
    }

}
