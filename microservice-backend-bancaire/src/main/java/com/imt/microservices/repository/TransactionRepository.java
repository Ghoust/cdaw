package com.imt.microservices.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import com.imt.microservices.entity.Transaction;

public interface TransactionRepository extends JpaRepository<Transaction, Long> {

    @Query("SELECT t FROM Transaction t where t.creditor.id = ?1 OR t.debtor.id = ?1")
    List<Transaction> findAllByCreditorIdOrDebtorIdEquals(Long id);

    @Transactional
    @Modifying
    @Query("DELETE FROM Transaction t where t.creditor.id = ?1 OR t.debtor.id = ?1")
    void deleteLinkedToAccount(Long id);
}
