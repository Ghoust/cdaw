package com.imt.microservices.service;

import java.util.List;
import java.util.Optional;

import org.springframework.stereotype.Service;

import com.imt.microservices.entity.Account;
import com.imt.microservices.repository.AccountRepository;
import com.imt.microservices.repository.TransactionRepository;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class AccountService {

    private final AccountRepository repository;
    private final TransactionRepository transactionRepository;
    private final ValidationService validator;

    public AccountService(AccountRepository repository, TransactionRepository transactionRepository, ValidationService validator) {
        this.repository = repository;
        this.transactionRepository = transactionRepository;
        this.validator = validator;
    }

    public List<Account> getAll() {
        return repository.findAll();
    }
    
    public Optional<Account> getById(Long id) {
        return repository.findById(id);
    }

    public Account addAccount(String iban) {
        if (!validator.validate(iban)) {
            return null;
        }
        Account account = new Account();
        account.setIban(iban);
        return repository.save(account);
    }

    public void deleteAccount(Long accountId) {
    	transactionRepository.deleteLinkedToAccount(accountId);
        repository.deleteById(accountId);
    }

}
