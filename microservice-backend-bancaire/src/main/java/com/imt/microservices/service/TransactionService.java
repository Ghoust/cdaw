package com.imt.microservices.service;

import com.imt.microservices.entity.Transaction;
import com.imt.microservices.repository.TransactionRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TransactionService {

    private final TransactionRepository repository;

    public TransactionService(TransactionRepository repository) {
        this.repository = repository;
    }

    public List<Transaction> getAll() {
        return repository.findAll();
    }

    public List<Transaction> getTransactionsByAccount(Long accountId) {
        return repository.findAllByCreditorIdOrDebtorIdEquals(accountId);
    }
}
