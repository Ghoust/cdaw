package com.imt.microservices.service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.imt.microservices.dto.IbanValidationResponse;
import com.imt.microservices.entity.Account;
import com.imt.microservices.entity.Transaction;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;

@Slf4j
@Service
public class ValidationService {

    private static final String API_IBAN_URL = "http://localhost:8080/validate/";

    public boolean validate(Transaction transaction) {
        Account debtor = transaction.getDebtor();
        Account creditor = transaction.getCreditor();
        return validate(debtor) && validate(creditor);
    }

    public boolean validate(Account account) {
        String iban = account.getIban();
        return validate(iban);
    }

    public boolean validate(String iban) {
        HttpClient client = HttpClient.newHttpClient();
        HttpRequest request = HttpRequest.newBuilder()
                .GET()
                .header("accept", "application/json")
                .uri(URI.create(API_IBAN_URL + iban))
                .build();
        HttpResponse<String> response;
        IbanValidationResponse validation = null;
        try {
            response = client.send(request, HttpResponse.BodyHandlers.ofString());
            ObjectMapper mapper = new ObjectMapper();
            validation = mapper.readValue(response.body(), new TypeReference<>() {
            });
        } catch (Exception e) {
            log.error("Erreur lors de la validation de l'iban : ", e);
        }

        if(validation == null) {
            return false;
        }
        return validation.isValid();
    }

}
