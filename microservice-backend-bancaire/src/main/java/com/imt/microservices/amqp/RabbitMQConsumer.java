package com.imt.microservices.amqp;

import com.imt.microservices.entity.Transaction;
import com.imt.microservices.repository.TransactionRepository;
import com.imt.microservices.service.ValidationService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class RabbitMQConsumer {

    private final TransactionRepository repository;
    private final ValidationService validator;

    public RabbitMQConsumer(TransactionRepository repository, ValidationService validator) {
        this.repository = repository;
        this.validator = validator;
    }

    @RabbitListener(queues = MessagingConfig.QUEUE_NAME)
    public void consumeMessageFromQueue(Transaction transaction) {
        log.info("Message receive from the queue : " + transaction);
        if (validator.validate(transaction)) {
            log.debug("Insertion de la transaction {} en BDD.", transaction);
            repository.save(transaction);
        } else {
            log.warn("Erreur lors de la validation de la transaction.");
        }
    }
}
