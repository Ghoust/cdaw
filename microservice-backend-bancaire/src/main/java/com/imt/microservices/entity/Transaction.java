package com.imt.microservices.entity;

import lombok.Data;

import javax.persistence.*;
import java.math.BigDecimal;

@Data
@Entity
@Table(name = "TRANSACTIONS")
public class Transaction {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    @Column(name = "ID")
    private Long id;

    @Column(name = "AMOUNT")
    private BigDecimal amount;

    @OneToOne
    @JoinColumn(name = "DEBTOR_ID")
    private Account debtor;

    @OneToOne
    @JoinColumn(name = "CREDITOR_ID")
    private Account creditor;

}
