package com.imt.microservices;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MicroserviceBackendBancaireApplication {

	public static void main(String[] args) {
		SpringApplication.run(MicroserviceBackendBancaireApplication.class, args);
	}

}
