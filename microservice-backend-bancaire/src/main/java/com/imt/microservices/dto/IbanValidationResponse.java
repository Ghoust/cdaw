package com.imt.microservices.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class IbanValidationResponse {

    private String iban;
    private boolean valid;

    public IbanValidationResponse(
            @JsonProperty(value = "iban", required = true) String iban,
            @JsonProperty(value = "valid", required = true) boolean valid
    ) {
        this.iban = iban;
        this.valid = valid;
    }

}
