/*
 * This Java source file was generated by the Gradle 'init' task.
 */
package org.imt.soap.client;

import org.junit.Test;
import static org.junit.Assert.*;

import org.imt.soap.client.Library;

public class LibraryTest {
    @Test public void testSomeLibraryMethod() {
        Library classUnderTest = new Library();
        assertTrue("someLibraryMethod should return 'true'", classUnderTest.someLibraryMethod());
    }
}
