package org.mines.douai.j2ee.webservices.main;
import static org.junit.Assert.assertEquals;

import java.net.URL;

import javax.xml.namespace.QName;
import javax.xml.ws.Service;

import org.imt.soap.client.HelloWorld;
import org.junit.Test;


public class HelloWorldConsumerTest {
	@Test
	public void sayHelloTest1() throws Exception
	{
		URL url = new URL("http://localhost:9000/helloWorld?wsdl");
		QName qname = new QName("http://soap.imt.org/", "HelloWorld");
		Service service = Service.create(url, qname);
		HelloWorld hello = service.getPort(HelloWorld.class);
		String sayHiResponse = hello.sayHi("Antoine");
		assertEquals("Hello Antoine", sayHiResponse);
	}
}
