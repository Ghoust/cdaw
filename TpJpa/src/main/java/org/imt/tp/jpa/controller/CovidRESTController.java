package org.imt.tp.jpa.controller;

import org.imt.tp.jpa.model.Covid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.persistence.EntityManager;
import java.util.List;

@RestController
@RequestMapping("/rest")
public class CovidRESTController {

	@Autowired
	private EntityManager entityManager;

	@GetMapping(value = { "/" }, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<Covid>> quoteAll() {
		List<Covid> result = entityManager.createQuery("from Covid", Covid.class).getResultList();
		for (Covid covid : result) {
			System.out.println(covid);
		}
		return new ResponseEntity<>(result, HttpStatus.OK);
	}

}
