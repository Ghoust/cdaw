package org.imt.tp.jpa.controller;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.imt.tp.jpa.form.SelectCountryForm;
import org.imt.tp.jpa.model.Country;
import org.imt.tp.jpa.model.Region;
import org.imt.tp.jpa.repository.CountryRepository;
import org.imt.tp.jpa.repository.RegionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/")
public class CovidIHMController {

	@Autowired
	private CountryRepository countryRepository;

	@Autowired
	private RegionRepository regionRepository;
	
	@GetMapping
	public String getCountries(Model model) {
		List<Country> countries = countryRepository.findAll();
		Set<Country> unsafeCountries = getAllUnsafe();
		model.addAttribute("countries", countries);
		model.addAttribute("unsafeCountries", unsafeCountries);
		model.addAttribute("form", new SelectCountryForm());
		return "index";
	}

	@PostMapping
	public String selectCountry(@ModelAttribute SelectCountryForm form, Model model) {
		return "redirect:country/"+form.getCountry();
	}
	
	public Set<Country> getAllUnsafe(){
		Set<Country> tempCountries;
		List<Region> regions = regionRepository.findAll();

		regions = regions.stream().filter(r -> r.getCaseCount().intValueExact() > 25000).collect(Collectors.toList());
		tempCountries = regions.stream().map(Region::getCountry).collect(Collectors.toSet());
		
		return tempCountries;
	}
}
