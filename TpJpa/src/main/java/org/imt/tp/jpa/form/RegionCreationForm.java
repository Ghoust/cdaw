package org.imt.tp.jpa.form;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class RegionCreationForm {

    private String name;
    private BigDecimal cases;

}
