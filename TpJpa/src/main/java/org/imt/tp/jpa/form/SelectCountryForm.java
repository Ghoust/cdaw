package org.imt.tp.jpa.form;

import lombok.Data;

@Data
public class SelectCountryForm {

    private String country;

}
