package org.imt.tp.jpa.controller;

import java.util.List;
import java.util.Optional;

import org.imt.tp.jpa.form.RegionCreationForm;
import org.imt.tp.jpa.model.Country;
import org.imt.tp.jpa.model.Region;
import org.imt.tp.jpa.repository.CountryRepository;
import org.imt.tp.jpa.repository.RegionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping("/country")
public class RegionController {

	@Autowired
	private CountryRepository countryRepository;

	@Autowired
	private RegionRepository regionRepository;

	@GetMapping("/{country}")
	public String getRegions(Model model, @PathVariable String country) {
		Country selectedCountry = countryRepository.findByName(country);
		List<Region> regions = regionRepository.findAllByCountry(selectedCountry);
		model.addAttribute("selectedCountry", selectedCountry.getName());
		model.addAttribute("regions", regions);
		return "country";
	}

	@PostMapping("/{country}")
	public String addRegion(@PathVariable String country, @ModelAttribute RegionCreationForm form) {
		Country entityCountry = countryRepository.findByName(country);
		Optional<Region> existingRegion = regionRepository.findByRegionName(form.getName());

		if(existingRegion.isPresent()) {
			Region newRegion = existingRegion.get();
			newRegion.setRegionName(form.getName());
			newRegion.setCaseCount(form.getCases());
			regionRepository.save(newRegion);
		} else {
			Region region = new Region();
			region.setCountry(entityCountry);
			region.setRegionName(form.getName());
			region.setCaseCount(form.getCases());
			regionRepository.save(region);
		}

		return "redirect:" + country;
	}

	@GetMapping("/{country}/delete/{regionId}")
	public String delete(@PathVariable String country, @PathVariable String regionId) {
		Optional<Region> entityRegion = regionRepository.findById(Long.parseLong(regionId));
		entityRegion.ifPresent(region -> regionRepository.delete(region));
		return "redirect:/country/" + country;
	}
}
