package org.imt.tp.jpa.model;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.Data;

@Data
@Entity
@Table(name="COVID")
public class Covid {
	@Id
	@Column(name="COVID_ID")
	private long id;

	@Column(name="CASE_COUNT")
	private BigDecimal caseCount;
	
	@ManyToOne
	@JoinColumn(name="COUNTRY_ID")
	private Country country;
}
