package org.imt.tp.jpa.repository;

import org.imt.tp.jpa.model.Country;
import org.imt.tp.jpa.model.Region;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface RegionRepository extends JpaRepository<Region, Long> {

    List<Region> findAllByCountry(Country country);

    Optional<Region> findByRegionName(String regionName);
}
