package org.imt.tp.jpa.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Data
@Entity
@Table(name = "COUNTRY")
public class Country {

	@Id
	@Column(name="COUNTRY_ID")
	private long id;
	
	@Column(name="NAME")
	private String name;
	
	@Column(name="PRESIDENT")
	private String president;

}
