package org.imt.tp.jpa.repository;

import org.imt.tp.jpa.model.Country;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CountryRepository extends JpaRepository<Country, Long> {

    Country findByName(String name);

}
