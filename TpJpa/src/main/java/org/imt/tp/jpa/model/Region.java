package org.imt.tp.jpa.model;

import java.math.BigDecimal;

import javax.persistence.*;

import lombok.Data;

@Data
@Entity
@Table(name="REGION")
public class Region {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	@Column(name="REGION_ID")
	private long id;
	
	@Column(name="REGION_NAME")
	private String regionName;
	
	@Column(name="REGION_CASE")
	private BigDecimal caseCount;

	@ManyToOne
	@JoinColumn(name="COUNTRY_ID")
	private Country country;
}
