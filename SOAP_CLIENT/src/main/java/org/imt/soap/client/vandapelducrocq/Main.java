package org.imt.soap.client.vandapelducrocq;

import java.net.URL;
import java.util.Date;

import javax.xml.datatype.DatatypeFactory;
import javax.xml.namespace.QName;
import javax.xml.ws.Service;


public class Main {

	public static void main(String[] args) throws Exception {
		// TODO Auto-generated method stub
		URL url = new URL("http://localhost:9000/vaccination?wsdl");
		QName qname = new QName("http://vandapelducrocq.webservices.j2ee.imt.org/", "VaccinationSoapWebService");
		Service service = Service.create(url, qname);
		Vaccination vaccination = service.getPort(Vaccination.class);
		Integer test = vaccination.getVaccination("a", DatatypeFactory.newInstance().newXMLGregorianCalendar("2021-01-05"));
		System.out.println(test);
	}

}
