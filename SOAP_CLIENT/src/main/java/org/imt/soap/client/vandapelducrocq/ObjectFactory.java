
package org.imt.soap.client.vandapelducrocq;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the org.imt.soap.client.vandapelducrocq package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _GetVaccinationResponse_QNAME = new QName("http://vandapelducrocq.webservices.j2ee.imt.org/", "getVaccinationResponse");
    private final static QName _GetVaccination_QNAME = new QName("http://vandapelducrocq.webservices.j2ee.imt.org/", "getVaccination");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: org.imt.soap.client.vandapelducrocq
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link GetVaccinationResponse }
     * 
     */
    public GetVaccinationResponse createGetVaccinationResponse() {
        return new GetVaccinationResponse();
    }

    /**
     * Create an instance of {@link GetVaccination }
     * 
     */
    public GetVaccination createGetVaccination() {
        return new GetVaccination();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetVaccinationResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://vandapelducrocq.webservices.j2ee.imt.org/", name = "getVaccinationResponse")
    public JAXBElement<GetVaccinationResponse> createGetVaccinationResponse(GetVaccinationResponse value) {
        return new JAXBElement<GetVaccinationResponse>(_GetVaccinationResponse_QNAME, GetVaccinationResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetVaccination }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://vandapelducrocq.webservices.j2ee.imt.org/", name = "getVaccination")
    public JAXBElement<GetVaccination> createGetVaccination(GetVaccination value) {
        return new JAXBElement<GetVaccination>(_GetVaccination_QNAME, GetVaccination.class, null, value);
    }

}
