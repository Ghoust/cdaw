<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="f" uri="http://java.sun.com/jsf/core"%>
<%@ taglib prefix="h" uri="http://java.sun.com/jsf/html"%>
<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title>TP - JSF</title>
	</head>
	<body>
		<h1>TP - JSF</h1>
		<f:view>
			<h:form>
				<h:outputLabel value="Select a country" for="countries"/><br>
				<h:selectOneListbox id="countries" value="#{covidBeanList.choice}" required="true">
					  <f:selectItems value="#{covidBeanList.getItems()}" />
				</h:selectOneListbox>
				<br>
				<h:commandButton value="submit" type="submit" action="#{covidBeanList.submit}" />
			</h:form>
			<br>
			<h:messages globalOnly="true" />
			<br>
			<h:graphicImage value="http://localhost:8080/COVIDTracker/GraphicCovid?country=#{covidBeanList.choice}"/>
		</f:view>
	</body>
</html>