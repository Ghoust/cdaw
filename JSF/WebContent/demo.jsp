<%@ page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="f"  uri="http://java.sun.com/jsf/core" %>
<%@ taglib prefix="h"  uri="http://java.sun.com/jsf/html" %>
<!DOCTYPE html>
<html>
	<head>
		<title>Exemple de JSF</title>
	</head>
	<body>
		<f:view>
			<h:form id="form" enctype="multipart/form-data">
				<h:inputText value="#{HelloWorldBean.name}" label="Nom" />
				<br>
				<h:commandButton value="Add" action="#{HelloWorldBean.addName()}" />
				<h:commandButton value="Clear" action="#{HelloWorldBean.clearName()}"/>
			</h:form>
		</f:view>
	</body>
</html>