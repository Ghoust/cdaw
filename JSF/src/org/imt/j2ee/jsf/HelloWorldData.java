package org.imt.j2ee.jsf;

public class HelloWorldData {

	private String name;

	public HelloWorldData() {
		this.name = "Francois";
	}
	
	public void addName() {
		name += " Ducrocq";
	}
	
	public void clearName() {
		name = "";
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
}
