/**
 * 
 */
package org.imt.j2ee.jsf.beans;

/**
 * Describes an item for the select list.
 * Contains the visual name of the country and the value for the select's 
 * option, alongside the current number of COVID cases (mock). 
 */
public class CovidBean {

	private String value;
	private String name;
	private Integer nbCases;
	
	public CovidBean(String name, String value, Integer nbCases) {
		this.name = name;
		this.value = value;
		this.nbCases = nbCases;
	}
	
	public String getName() {
		return name;
	}

	public String getValue() {
		return value;
	}

	public Integer getNbCases() {
		return nbCases;
	}
}
