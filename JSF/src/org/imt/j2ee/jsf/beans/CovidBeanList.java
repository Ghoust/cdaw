package org.imt.j2ee.jsf.beans;

import java.util.ArrayList;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;

/**
 * Describes the select input used in the jsp.
 * Handles the submit operation and acts as the model for the view.
 * @author devdu
 *
 */
public class CovidBeanList {

	private List<CovidBean> beans;
	
	private String choice;
	private Integer choiceCount;

	public CovidBeanList() {
		beans = new ArrayList<>();
		beans.add(new CovidBean("France", "France", 2639773));
		beans.add(new CovidBean("Allemagne", "Allemagne", 1762637));
		beans.add(new CovidBean("USA", "USA", 20173382));
	}
	
	/**
	 * Methods used for populating the select input with options.
	 * @return A list of {@code SelectItem}
	 */
	public List<SelectItem> getItems() {
		List<SelectItem> items = new ArrayList<>();
		for (CovidBean bean : beans) {
			SelectItem item = new SelectItem(bean.getValue(), bean.getName());
			items.add(item);
		}
		return items;
	}
	
	/**
	 * Operations after submitting the form
	 * Prints the number of current cases of a country in a global message.
	 */
	public void submit() {
		for (CovidBean bean : beans) {
			if (bean.getValue().equals(choice)) {
				choiceCount = bean.getNbCases();
			}
		}
		FacesMessage message = new FacesMessage(choice + " : " + choiceCount + " cases.");
		FacesContext.getCurrentInstance().addMessage(null, message);
	}
	
	public Integer getChoiceCount() {
		return choiceCount;
	}
	
	public String getChoice() {
		return choice;
	}
	
	public void setChoice(String choice) {
		this.choice = choice;
	}
	
}
