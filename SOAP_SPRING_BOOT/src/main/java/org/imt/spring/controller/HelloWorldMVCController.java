package org.imt.spring.controller;

import java.net.URL;

import javax.xml.datatype.DatatypeFactory;
import javax.xml.namespace.QName;
import javax.xml.ws.Service;

import org.imt.spring.model.Vaccination;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/")
public class HelloWorldMVCController {
	
	@GetMapping("/vaccination")
	public String vaccination(Model model) throws Exception {
		URL url = new URL("http://localhost:9000/vaccination?wsdl");
		QName qname = new QName("http://vandapelducrocq.webservices.j2ee.imt.org/", "VaccinationSoapWebService");
		Service service = Service.create(url, qname);
		Vaccination vaccination = service.getPort(Vaccination.class);
		Integer test = vaccination.getVaccination("a", DatatypeFactory.newInstance().newXMLGregorianCalendar("2021-01-05"));
		System.out.println(test);
		model.addAttribute("vaccination", test);
		return "vaccination.html";
	}
}
