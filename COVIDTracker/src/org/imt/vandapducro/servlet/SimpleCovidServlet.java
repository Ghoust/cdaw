package org.imt.vandapducro.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.AbstractMap;
import java.util.Map;
import java.util.Map.Entry;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class SimpleCovidServlet
 */
@WebServlet("/SimpleCovid")
public class SimpleCovidServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	private Map<String, Long> mapCase;
	
    /**
     * @see HttpServlet#HttpServlet()
     */
    public SimpleCovidServlet() {
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		mapCase = Map.ofEntries(
				new AbstractMap.SimpleEntry<String, Long>("USA", (long) 10000000 + (long) (Math.random() * (15000000 - 10000000))),
				new AbstractMap.SimpleEntry<String, Long>("ALLEMAGNE", (long) 2500000 + (long) (Math.random() * (3500000 - 2500000))),
				new AbstractMap.SimpleEntry<String, Long>("FRANCE", (long) 1_376_852 + (long) (Math.random() * (3_376_852 - 1_376_852)))
		);
		
		PrintWriter writer = response.getWriter();
		writer.append("<html>");
		writer.append("    <head>");
		writer.append("    <meta charset='UTF-8'>");
		writer.append("    </head>");
		writer.append("    <body>");
		writer.append("        <form action='/COVIDTracker/SimpleCovid' method='GET'>");
		writer.append("        <input type='submit' value='Refresh'>");
		writer.append("        </form>");
		for (Entry<String, Long> entry : mapCase.entrySet()) {
			writer.append("        <p>Nombre de cas en " + entry.getKey() + " = " + entry.getValue() + "</p>");
		}
		writer.append("        <form action='/COVIDTracker/SimpleCovid' method='POST'");
		writer.append("        <label for='country'>Pays :</label>");
		writer.append("        <select name='country' id='country'>");
		for (Entry<String, Long> entry : mapCase.entrySet()) {
			if(entry.getKey().equals(request.getParameter("country"))) {
				writer.append("            <option selected value='"+entry.getKey()+"'>"+entry.getKey()+"</option>");
			}
			else {
				writer.append("            <option value='"+entry.getKey()+"'>"+entry.getKey()+"</option>");
			}
		}
		writer.append("        </select>");
		writer.append("        <input type='submit' value='GO'>");
		writer.append("        </form>");
		
		writer.append("    </body>");
		writer.append("</html>");
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
		PrintWriter writer = response.getWriter();

		String country = request.getParameter("country");
		Long count;
		switch (country.toUpperCase()) {
		case "ALLEMAGNE":
			count = mapCase.get("ALLEMAGNE");
			break;
		case "FRANCE":
			count = mapCase.get("FRANCE");
			break;
		case "USA":
			count = mapCase.get("USA");
			break;
		default:
			count = (long) -1;
			break;
		}
		
		writer.append("Nombre de cas : " + count);
		writer.append("<div><img src='/COVIDTracker/GraphicCovid?country=" + country + "'></div>");
	}

}
