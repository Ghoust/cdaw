package org.imt.vandapducrocq.graphic.servlet;

import java.io.IOException;
import java.io.OutputStream;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import javax.imageio.ImageIO;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class GraphicCovidServlet
 * 
 * L�un des int�r�ts principaux de l�utilisation d�une servlet est de pouvoir 
 * g�n�rer autre chose que du HTML, car pour le HTML, on utilise souvent les 
 * JSPs / Tags Libs, afin de ne pas m�langer les couches.
 * 
 * Dans cette partie, vous allez faire une servlet qui affiche une courbe des 
 * cas de Covid (sous forme de valeurs al�atoires) au format png, ainsi que
 * l�affichage simplifi� du logo associ�.
 * L�abscisse sera le temps (30 derniers jours) et l�ordonn�e le classement.
 */
@WebServlet("/GraphicCovid")
public class GraphicCovidServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	private List<Integer> usaCase = Arrays.asList(10, 20, 20, 12, 20, 19, 17, 18, 15 ,14
			, 13, 14, 13, 14, 14, 14, 15, 14, 15, 16
			, 16, 17, 19, 13, 17, 1, 1, 19, 14, 12);
	private List<Integer> germanyCase = Arrays.asList(1, 4, 2, 2, 1, 5, 9, 20, 21, 20
			, 14, 15, 12, 9, 10, 11, 9, 8, 8, 6
			, 6, 7, 9, 15, 5, 3, 3, 3, 2, 1);
	private List<Integer> franceCase = Arrays.asList(1, 3, 5, 9, 8, 9, 7, 8, 5 ,4
			, 3, 4, 3, 4, 4, 4, 5, 4, 5, 6
			, 6, 7, 9, 13, 17, 20, 21, 19, 17, 15);
	
    /**
     * @see HttpServlet#HttpServlet()
     */
    public GraphicCovidServlet() {
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		OutputStream output = response.getOutputStream();
		response.setContentType("image/jpeg");
		
		String country = request.getParameter("country");
		Graph graph = null;
		switch (country.toUpperCase()) {
		case "ALLEMAGNE":
			graph = new Graph(germanyCase);
			break;
		case "FRANCE":
			graph = new Graph(franceCase);
			break;
		case "USA":
			graph = new Graph(usaCase);
			break;
		default:
			graph = new Graph(Collections.emptyList());
		}
		
		graph.create();
		ImageIO.write(graph.bufferedImage, "jpeg", output);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
