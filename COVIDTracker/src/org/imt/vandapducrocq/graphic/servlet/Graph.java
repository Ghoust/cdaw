package org.imt.vandapducrocq.graphic.servlet;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.Stroke;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.List;

public class Graph {

	private static final int IMAGE_WIDTH = 600;
	private static final int IMAGE_HEIGHT = 600;
	
	private static final int MAX_SCORE = 20;
	private static final int BORDER_GAP = 30;
	private static final Color GRAPH_COLOR = Color.green;
	private static final Color GRAPH_POINT_COLOR = new Color(150, 50, 50, 180);
	private static final Stroke GRAPH_STROKE = new BasicStroke(3f);
	private static final int GRAPH_POINT_WIDTH = 12;
	private static final int Y_HATCH_CNT = 10;
	
	public BufferedImage bufferedImage;
	
	private List<Integer> scores;
   
	public Graph(List<Integer> scores) {
		this.scores = scores;
	}
   
	public void create() {
		bufferedImage = new BufferedImage(IMAGE_WIDTH, IMAGE_HEIGHT, BufferedImage.TYPE_INT_RGB);
		Graphics2D graph = bufferedImage.createGraphics(); 
		graph.drawLine(BORDER_GAP, IMAGE_WIDTH - BORDER_GAP, BORDER_GAP, BORDER_GAP);
		graph.drawLine(BORDER_GAP, IMAGE_HEIGHT - BORDER_GAP, IMAGE_WIDTH- BORDER_GAP, IMAGE_HEIGHT - BORDER_GAP);
		
		double xScale = ((double) IMAGE_WIDTH - 2 * BORDER_GAP) / (scores.size() - 1);
		double yScale = ((double) IMAGE_HEIGHT - 2 * BORDER_GAP) / (MAX_SCORE - 1);

		
		List<Point> graphPoints = new ArrayList<Point>();
		for (int i = 0; i < scores.size(); i++) {
			int x1 = (int) (i * xScale + BORDER_GAP);
			int y1 = (int) ((MAX_SCORE - scores.get(i)) * yScale + BORDER_GAP);
			graphPoints.add(new Point(x1, y1));
		}
		
		// create hatch marks for y axis. 
		for (int i = 0; i < Y_HATCH_CNT; i++) {
			int x0 = BORDER_GAP;
			int x1 = GRAPH_POINT_WIDTH + BORDER_GAP;
			int y0 = IMAGE_HEIGHT - (((i + 1) * (IMAGE_HEIGHT - BORDER_GAP * 2)) / Y_HATCH_CNT + BORDER_GAP);
			int y1 = y0;
			graph.drawLine(x0, y0, x1, y1);
		}
		
		// create hatch marks for x axis
		for (int i = 0; i < scores.size() - 1; i++) {
			int x0 = (i + 1) * (IMAGE_WIDTH - BORDER_GAP * 2) / (scores.size() - 1) + BORDER_GAP;
			int x1 = x0;
			int y0 = IMAGE_HEIGHT - BORDER_GAP;
			int y1 = y0 - GRAPH_POINT_WIDTH;
			graph.drawLine(x0, y0, x1, y1);
		}
		
		Stroke oldStroke = graph.getStroke();
	      graph.setColor(GRAPH_COLOR);
	      graph.setStroke(GRAPH_STROKE);
	      for (int i = 0; i < scores.size() - 1; i++) {
	         int x1 = graphPoints.get(i).x;
	         int y1 = graphPoints.get(i).y;
	         int x2 = graphPoints.get(i + 1).x;
	         int y2 = graphPoints.get(i + 1).y;
	         graph.drawLine(x1, y1, x2, y2);         
	      }

	      graph.setStroke(oldStroke);      
	      graph.setColor(GRAPH_POINT_COLOR);
	      for (int i = 0; i < scores.size(); i++) {
	         int x = graphPoints.get(i).x - GRAPH_POINT_WIDTH / 2;
	         int y = graphPoints.get(i).y - GRAPH_POINT_WIDTH / 2;;
	         int ovalW = GRAPH_POINT_WIDTH;
	         int ovalH = GRAPH_POINT_WIDTH;
	         graph.fillOval(x, y, ovalW, ovalH);
	      }
		
	}

}
