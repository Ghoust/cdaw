<%@ page import="java.util.Map, java.util.Map.Entry, java.util.AbstractMap" %>

<!DOCTYPE html>
<html>
	<head>
		<meta charset="ISO-8859-1">
		<title>COVID TRACKER JSP</title>
	</head>
	<body>	
		<% Map<String, Long> mapCase = Map.ofEntries(
				new AbstractMap.SimpleEntry<String, Long>("USA", (long) 10000000 + (long) (Math.random() * (15000000 - 10000000))),
				new AbstractMap.SimpleEntry<String, Long>("ALLEMAGNE", (long) 2500000 + (long) (Math.random() * (3500000 - 2500000))),
				new AbstractMap.SimpleEntry<String, Long>("FRANCE", (long) 1_376_852 + (long) (Math.random() * (3_376_852 - 1_376_852)))
		);%>

		<form action='/COVIDTracker/SimpleCovidJSP' method='GET'>
			<input type='submit' value='Refresh'>
		</form>
		
		<% for (Entry<String, Long> entry : mapCase.entrySet()) { %>
			<p>Nombre de cas en <%= entry.getKey() %> = <%= entry.getValue() %></p>
		<% } %>
		
		<form action='/COVIDTracker/SimpleCovidJSP' method='POST'>
			<label for='country'>Pays :</label>
			<select name='country' id='country'>
			<% for (Entry<String, Long> entry : mapCase.entrySet()) { %>
				<% if (entry.getKey().equals(request.getParameter("country"))) {%>
					<option selected value='<%= entry.getKey() %>'><%= entry.getKey() %></option>
				<% } else { %>
					<option value='<%= entry.getKey() %>'><%= entry.getKey() %></option>
				<% } %>	
			<% } %>
			</select>
			<input type='submit' value='GO'>
		</form>
		
		
		<% String country = (String) request.getParameter("country"); %>
		<% if (country != null) {%>
			<% Long count; %>
			<% switch (country) {
				case "ALLEMAGNE":
					count = mapCase.get("ALLEMAGNE");
					break;
				case "FRANCE":
					count = mapCase.get("FRANCE");
					break;
				case "USA":
					count = mapCase.get("USA");
					break;
				default:
					count = (long) -1;
					break;
			} %>	
			Nombre de cas : <%=count%>
		<% } %>
		
	</body>
</html>
