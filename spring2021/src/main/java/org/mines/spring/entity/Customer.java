package org.mines.spring.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;



@Entity
@Table(name="CUSTOMERS")
public class Customer {
	
	@Id
	@Column(name = "CUSTOMER_ID")
	private Long id;
	
	@Column(name = "NAME")
	private String name;
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Long getId() {
		return id;
	}
	
	protected void setId (Long id) {
		this.id = id;
	}
	
	
	
	@Override
    public String toString() {
        return "Customer [id=" + id + ", name=" + name + "]";
    }

}