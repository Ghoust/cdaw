package org.mines.spring.pojo;

import org.springframework.stereotype.Component;

@Component
public class HelloWorld {

	private String greeting;
	
	private String name;

	public HelloWorld() {
		
	}
	
	public HelloWorld(String name) {
		this.name = name;
	}
	
	public HelloWorld(String greeting, String name) {
		this.greeting = greeting;
		this.name = name;
	}
	
	public String getGreeting() {
		return greeting;
	}

	public void setGreeting(String greeting) {
		this.greeting = greeting;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
}
