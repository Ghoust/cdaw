package org.mines.spring.controller;

import org.mines.spring.pojo.HelloWorld;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/rest")
public class HelloWorldController {

	@GetMapping("/helloworld/{name}")
	public HelloWorld getHelloWorld(@PathVariable String name) {
		return new HelloWorld("Hello", name);
	}
	
}
