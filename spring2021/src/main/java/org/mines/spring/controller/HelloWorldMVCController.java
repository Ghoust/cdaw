package org.mines.spring.controller;

import org.mines.spring.pojo.HelloWorld;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/mvc")
public class HelloWorldMVCController {

	@GetMapping("/helloworld")
	public String helloworld(Model model) {
		HelloWorld hw = new HelloWorld("Hello", "Fran�ois");
		model.addAttribute("helloworld", hw);
		return "helloworld.html";
	}
	
}
