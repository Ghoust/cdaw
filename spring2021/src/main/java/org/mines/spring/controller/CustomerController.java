package org.mines.spring.controller;

import java.util.List;
import java.util.Optional;

import javax.persistence.EntityManager;

import org.mines.spring.entity.Customer;
import org.mines.spring.repository.CustomerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/customer")
public class CustomerController {

	@Autowired
	private EntityManager entityManager;
	
	@Autowired
	private CustomerRepository customerRepository;
	
	@GetMapping(value = { "/" }, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<Customer>> quoteAll() {
		List<Customer> result = entityManager.createQuery("from Customer", Customer.class).getResultList();
		for (Customer customer : result) {
			System.out.println(customer);
		}
		return new ResponseEntity<List<Customer>>(result, HttpStatus.OK);
	}
	
	@GetMapping(value = { "/{customerName}" }, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Customer> customerByName(@PathVariable String customerName) {
		Optional<Customer> customer = customerRepository.findByName(customerName);
		if (customer.isPresent()) {
			Customer c = customer.get();
			System.out.println(c);
			return new ResponseEntity<Customer>(c, HttpStatus.OK);
		}
		
		return new ResponseEntity<Customer>(HttpStatus.OK);
	}
	
}
