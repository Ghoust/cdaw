package org.mines.spring;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mines.spring.pojo.HelloWorld;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.ApplicationContext;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

@ExtendWith(SpringExtension.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.MOCK, classes = SpringDemoApplication.class)
@AutoConfigureMockMvc
class SpringDemoApplicationTests {

	@Autowired
	protected ApplicationContext ctx;
	
	@Autowired
	private MockMvc mvc;
	
	@Test
	void contextLoads() throws Exception {
		HelloWorld h = new HelloWorld("Hello", "Fran�ois");
		System.out.println("Sans spring boot : " + h);
		
		HelloWorld h2 = ctx.getBean(HelloWorld.class);
		System.out.println("Avec spring boot : " + h2);
		
		// Test d'une URL
		mvc.perform(MockMvcRequestBuilders
				.get("/rest/helloworld/francois")
				.accept(MediaType.APPLICATION_JSON))
				.andExpect(MockMvcResultMatchers.status().isOk());
	}

}
