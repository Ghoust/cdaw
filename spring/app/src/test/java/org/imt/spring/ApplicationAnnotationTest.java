package org.imt.spring;

import org.imt.spring.annotation.BeanOne;
import org.imt.spring.annotation.BeanTwo;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class ApplicationAnnotationTest {
	@Test
	public void should_simple_bean_be_injected() {
		ApplicationContext applicationContext = new ClassPathXmlApplicationContext("application-annotation-context.xml");
		
		BeanTwo beanTwo = (BeanTwo) applicationContext.getBean(BeanTwo.class);
		BeanOne beanOne = beanTwo.getBeanOne();
		
		Assertions.assertNotNull(beanOne);
	}
}
