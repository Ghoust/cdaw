package org.imt.spring.tp;

import java.math.BigDecimal;

public interface CovidServiceBean {
	public BigDecimal getCaseCount(String country);
}
