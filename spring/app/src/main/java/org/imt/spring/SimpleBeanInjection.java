package org.imt.spring;

public class SimpleBeanInjection {
	private SimpleBean simpleBean;
	
	public void setSimpleBean(SimpleBean simpleBean) {
		this.simpleBean = simpleBean;
	}

	public SimpleBean getSimpleBean() {
		return simpleBean;
	}
	
	
}
