package org.imt.spring.annotation;

import javax.inject.Inject;

import org.springframework.stereotype.Component;

@Component
public class BeanTwo {
	@Inject
	private BeanOne beanOne;

	public BeanOne getBeanOne() {
		return beanOne;
	}

	public void setBeanOne(BeanOne beanOne) {
		this.beanOne = beanOne;
	}
	
	
}
