package org.imt.spring.tp;

public class CovidBeanImpl implements CovidBean{
	private String name;
	
	
	
	public String getName() {
		return name;
	}



	public void setName(String name) {
		this.name = name;
	}

	public CovidBeanImpl init() {
		return new CovidBeanImpl();
	}

	@Override
	public void printCovidCase() {
		System.out.println(this.getName() +":"+this.hashCode());
	}
}
