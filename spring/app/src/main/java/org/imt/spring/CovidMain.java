package org.imt.spring;

import org.imt.spring.tp.CovidBean;
import org.imt.spring.tp.CovidBeanImpl;
import org.imt.spring.tp.annotation.AnnotationCovidBeanImpl;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class CovidMain {
	static CovidBeanImpl frbean = new CovidBeanImpl();
	static CovidBeanImpl enbean = new CovidBeanImpl();
	static CovidBeanImpl debean = new CovidBeanImpl();
	static AnnotationCovidBeanImpl frbeanAnnotation = new AnnotationCovidBeanImpl();
	static AnnotationCovidBeanImpl enbeanAnnotation = new AnnotationCovidBeanImpl();
	static AnnotationCovidBeanImpl debeanAnnotation = new AnnotationCovidBeanImpl();
	
	public static void main(String[] args) {
		ApplicationContext applicationContext = new ClassPathXmlApplicationContext("application-context.xml");
		CovidBean bean = applicationContext.getBean("covidBean", CovidBeanImpl.class);
		bean.printCovidCase();
		
		
		frbean = applicationContext.getBean("covidBean", CovidBeanImpl.class);
		frbean.setName("FR");
		enbean = applicationContext.getBean("covidBean", CovidBeanImpl.class);
		enbean.setName("EN");
		debean = applicationContext.getBean("covidBean", CovidBeanImpl.class);
		debean.setName("DE");
		printCaseOfCountries();
		((ConfigurableApplicationContext) applicationContext).close();
		
		System.out.println("--Version annotation--");
		
		ApplicationContext applicationAnnotationContext = new ClassPathXmlApplicationContext("application-annotation-context.xml");		
		frbeanAnnotation = applicationAnnotationContext.getBean(AnnotationCovidBeanImpl.class);
		frbeanAnnotation.setName("FR");
		enbeanAnnotation = applicationAnnotationContext.getBean(AnnotationCovidBeanImpl.class);
		enbeanAnnotation.setName("EN");
		debeanAnnotation = applicationAnnotationContext.getBean(AnnotationCovidBeanImpl.class);
		debeanAnnotation.setName("DE");
		printCaseOfCountriesAnnotation();
		((ConfigurableApplicationContext) applicationAnnotationContext).close();
		
	}
	
	public static void printCaseOfCountries() {
		frbean.printCovidCase();
		enbean.printCovidCase();
		debean.printCovidCase();
	}
	
	public static void printCaseOfCountriesAnnotation() {
		frbeanAnnotation.printCovidCase();
		enbeanAnnotation.printCovidCase();
		debeanAnnotation.printCovidCase();
	}
}
