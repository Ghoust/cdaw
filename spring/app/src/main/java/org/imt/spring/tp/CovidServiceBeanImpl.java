package org.imt.spring.tp;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

public class CovidServiceBeanImpl implements CovidServiceBean{
	private Map<String,Integer> values;
	
	public CovidServiceBeanImpl() {
		this.values = new HashMap<>();
		values.put("FR", 15340);
		values.put("DE", 61200);
		values.put("EN", 101690);
	}
	
	@Override
	public BigDecimal getCaseCount(String country) {
		System.out.println(this.values.get(country));
		Integer temp = this.values.get(country);
		this.values.replace(country, temp+1);
		System.out.println(this.values.get(country));
		return(BigDecimal.valueOf(temp));
	}
}
