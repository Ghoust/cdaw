package org.imt.spring.tp.annotation;

import javax.annotation.PostConstruct;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope("prototype")
public class AnnotationCovidBeanImpl implements AnnotationCovidBean{
	private String name;
	
	
	
	public String getName() {
		return name;
	}



	public void setName(String name) {
		this.name = name;
	}

	@PostConstruct
	public AnnotationCovidBeanImpl init() {
		return new AnnotationCovidBeanImpl();
	}

	@Override
	public void printCovidCase() {
		System.out.println(this.getName() +":"+this.hashCode());
	}
}
