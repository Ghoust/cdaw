package org.mines.douai.j2ee.taglib;

import java.io.IOException;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.TagSupport;

public class HelloTag extends TagSupport {
	protected String nom;
	
	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public int doStartTag() throws JspException {
		try {
			JspWriter out = pageContext.getOut();
			if (nom == null || nom.equals("")) {
				out.println("Hello World !");
			} else {
				out.println("Bonjour " +nom);
			}
		} catch (IOException e) {
			throw new JspException ("I/O Error", e);
		}
		return SKIP_BODY;
	}
}