package org.imt

import io.gatling.core.Predef._
import io.gatling.core.structure.ScenarioBuilder
import io.gatling.http.Predef._
import io.gatling.http.protocol.HttpProtocolBuilder

import scala.concurrent.duration.DurationInt

class BankSimulation extends Simulation {

  val httpProtocol: HttpProtocolBuilder = http
    .baseUrl("http://localhost:8082")
    .acceptHeader("text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8")
    .doNotTrackHeader("1")
    .acceptLanguageHeader("en-US,en;q=0.5")
    .acceptEncodingHeader("gzip, deflate")
    .userAgentHeader("Mozilla/5.0 (Macintosh; Intel Mac OS X 10.8; rv:16.0) Gecko/20100101 Firefox/16.0")

  // A scenario is a chain of requests and pauses
  val scn: ScenarioBuilder = scenario("Utilisation de l'application lors d'une montée en charge")
    .exec(
      http("Consultation de tous les comptes")
        .get("/account/")
        .check(status.is(200))
    )
    .exec(
      http("Consultation d'un compte")
        .get("/account/1/transactions")
        .check(status.is(200))
    )
    .exec(
      http("Ajout d'un compte")
        .post("/account/?iban=FR1420041010050500013M02606")
        .check(status.is(200))
    )
    .exec(
      http("Consultation de toutes les transactions")
        .get("/transactions/")
        .check(status.is(200))
    )

  setUp(
    scn.inject(
      constantConcurrentUsers(200).during(1.minute))
  ).protocols(httpProtocol)
}
