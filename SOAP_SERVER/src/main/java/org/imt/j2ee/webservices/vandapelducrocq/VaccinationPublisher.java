package org.imt.j2ee.webservices.vandapelducrocq;

import javax.xml.ws.Endpoint;

public class VaccinationPublisher {
	public static void main(String[] args) {
		System.out.println("Starting Server");
		VaccinationImpl implementor = new VaccinationImpl();
		String address = "http://localhost:9000/vaccination";
		Endpoint.publish(address, implementor);
	}
}
