package org.imt.j2ee.webservices.vandapelducrocq;

import java.util.Date;

import javax.jws.WebParam;
import javax.jws.WebService;

@WebService
public interface Vaccination {
	public Integer getVaccination (@WebParam(name="country") String country,@WebParam(name="date") Date value);
}
