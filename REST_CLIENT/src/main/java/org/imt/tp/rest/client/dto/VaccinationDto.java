package org.imt.tp.rest.client.dto;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class VaccinationDto {

	private String country;
	private int vaccinated;

	@JsonCreator
	public VaccinationDto(
			@JsonProperty(value = "country", required = true) String country, 
			@JsonProperty(value = "vaccinated", required = true) int vaccinated) {
		this.country = country;
		this.vaccinated = vaccinated;
	}
}
