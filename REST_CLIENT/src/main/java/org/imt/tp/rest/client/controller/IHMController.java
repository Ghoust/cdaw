package org.imt.tp.rest.client.controller;

import java.io.IOException;
import java.text.Normalizer;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import org.imt.tp.rest.client.dto.Form;
import org.imt.tp.rest.client.dto.VaccinationDto;
import org.imt.tp.rest.client.service.VaccinationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/")
public class IHMController {

	@Autowired
	private VaccinationService service; 
	
	@GetMapping
	public String getAll(Model model) {
		List<VaccinationDto> vaccinations = Collections.emptyList();
		try {
			vaccinations = service.getAll();
		} catch (IOException | InterruptedException e) {
			e.printStackTrace();
		}
		model.addAttribute("form", new Form());
		model.addAttribute("vaccinations", vaccinations);
		return "index.html";
	}

	@PostMapping
	public String getByCountry(@ModelAttribute Form form, Model model) {
		VaccinationDto vaccination = null;
		try {
			vaccination = service.getByCountry(form.getCountry());
		} catch (IOException | InterruptedException e) {
			e.printStackTrace();
		}

		if(vaccination != null) {
			model.addAttribute("result", vaccination);
		}

		return "details.html";
	}
}
