package com.imt.microservices.iban.dto;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class Iban {

	private String code;
	private boolean valid;

	@JsonCreator
	public Iban(
			@JsonProperty(value = "code", required = true) String code,
			@JsonProperty(value = "valid", required = true) boolean valid) {
		this.code = code;
		this.valid = valid;
	}
}
