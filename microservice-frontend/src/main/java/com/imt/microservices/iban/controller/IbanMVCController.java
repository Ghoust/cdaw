package com.imt.microservices.iban.controller;

import java.io.IOException;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.imt.microservices.iban.dto.Form;
import com.imt.microservices.iban.dto.Iban;
import com.imt.microservices.iban.service.IbanService;

@Controller
@RequestMapping("/")
public class IbanMVCController {

	private final IbanService service;

	public IbanMVCController(IbanService service) {
		this.service = service;
	}

	@GetMapping
	public String iban() {
		return "iban";
	}
	
	@PostMapping
	public String verificationIban(Form form, Model model) {
		Iban result = null;
		if(form.getIban().trim().length() > 0) {
			try {
				result = service.getValidationIban(form.getIban());
			} catch (IOException | InterruptedException e) {
				e.printStackTrace();
			}
		}
		else {
			result = new Iban("",false);
		}


		if(result != null) {
			model.addAttribute("result", result);
		}

		return "iban";
	}
}
