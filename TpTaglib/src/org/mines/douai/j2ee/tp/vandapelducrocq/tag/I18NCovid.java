package org.mines.douai.j2ee.tp.vandapelducrocq.tag;

import java.io.IOException;
import java.util.Locale;
import java.util.ResourceBundle;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.TagSupport;

public class I18NCovid extends TagSupport {
	protected String lang;
	protected String key;

	public String getLang() {
		return lang;
	}

	public void setLang(String lang) {
		this.lang = lang;
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public int doStartTag() throws JspException {
		try {
			JspWriter out = pageContext.getOut();
			out.println("Texte avec locale (en,US) :");
			Locale localeEN = new Locale("en", "US");
			ResourceBundle messageEN = ResourceBundle.getBundle("ButtonLabel", localeEN);
			out.println(messageEN.getString("Greeting"));
			
			out.println("// Texte avec locale (fr,FR) :");
			Locale localeFR = new Locale("fr", "FR");
			ResourceBundle messageFR = ResourceBundle.getBundle("ButtonLabel", localeFR);
			out.println(messageFR.getString("Greeting"));
		} catch (IOException e) {
			throw new JspException ("I/O Error", e);
		}
		return SKIP_BODY;
	}
}