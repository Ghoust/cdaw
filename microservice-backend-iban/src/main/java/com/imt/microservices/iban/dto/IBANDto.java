package com.imt.microservices.iban.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class IBANDto {

	private String iban;
	private boolean isValid;
	
}
