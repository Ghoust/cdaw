package com.imt.microservices.iban.service;

import org.apache.commons.validator.routines.IBANValidator;
import org.springframework.stereotype.Service;

@Service
public class IBANValidatorService {

	public boolean validate(String iban) {
		IBANValidator validator = new IBANValidator();
		return validator.isValid(iban);
	}
	
}
