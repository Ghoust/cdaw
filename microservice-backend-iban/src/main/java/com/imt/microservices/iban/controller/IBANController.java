package com.imt.microservices.iban.controller;

import com.imt.microservices.iban.dto.IBANDto;
import com.imt.microservices.iban.service.IBANValidatorService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/validate")
public class IBANController {

	private final IBANValidatorService service;

	public IBANController(IBANValidatorService service) {
		this.service = service;
	}

	@GetMapping("/{iban}")
	public IBANDto validate(@PathVariable String iban) {
		boolean isValid = service.validate(iban); 
		return new IBANDto(iban, isValid);
	}
	
}
