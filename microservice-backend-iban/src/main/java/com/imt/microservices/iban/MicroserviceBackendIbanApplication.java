package com.imt.microservices.iban;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MicroserviceBackendIbanApplication {

	public static void main(String[] args) {
		SpringApplication.run(MicroserviceBackendIbanApplication.class, args);
	}

}
